// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Builder/Builder/CanGenerateCity.h"
#include "GenerationDirector.generated.h"

/**
 * A director that know how to create different type of citties
 */
UCLASS()
class BUILDER_API UGenerationDirector : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	/** Step by step generaters elven city */
	static void GenerateCity(ICanGenerateCity* Generator);

};

inline void UGenerationDirector::GenerateCity(ICanGenerateCity* Generator)
{
	if(Generator)
	{
		Generator->GenerateGrounds();
		Generator->GenerateCastle();
		Generator->GenerateHourse();
	}
}